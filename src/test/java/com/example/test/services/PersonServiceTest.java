package com.example.test.services;

import com.example.test.dto.Persona;
import com.example.test.model.Cars;
import com.example.test.model.Person;
import com.example.test.repository.PersonRepository;
import com.example.test.services.impl.PersonService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Mock
    private PersonService personService;


    @Test
    public void findAllTest(){
        List<Person> lsPerson = new ArrayList<>();
        List<Cars> LsCar1 = new ArrayList<>();
        List<Cars> LsCar2 = new ArrayList<>();
        var cars1 = new Cars();
        cars1.setId(1L);
        cars1.setName("BMW");
        LsCar1.add(cars1);
        var cars2 = new Cars();
        cars2.setId(5L);
        cars2.setName("Ford");
        LsCar2.add(cars2);
        var person1 = new Person();
        person1.setId(1L);
        person1.setFirstName("Bob");
        person1.setSecondName("Fill");
        person1.setCarsList(LsCar1);
        var person2 = new Person();
        person2.setId(4L);
        person2.setFirstName("Joe");
        person2.setSecondName("Still");
        person2.setCarsList(LsCar2);

        List<Persona> lsPersona = new ArrayList<>();
        List<String> LsCars1 = new ArrayList<>();
        LsCars1.add("BMW");
        List<String> LsCars2 = new ArrayList<>();
        LsCars2.add("Ford");
        var persona1 = new Persona();
        persona1.setPersons_Id(1L);
        persona1.setPersonName("Bob");
        persona1.setCarNameList(LsCars1);
        var persona2 = new Persona();
        persona2.setPersons_Id(4L);
        persona2.setPersonName("Joe");
        persona2.setCarNameList(LsCars2);

        lsPerson.add(person1);
        lsPerson.add(person2);

        var list = personService.calculate(lsPerson);

        assertEquals(lsPersona, list);

    }
}
