package com.example.test.services;

import com.example.test.dto.Persona;
import com.example.test.model.Person;

import java.util.List;

public interface PersonServiceI {

    Persona findById(Long aLong);

    List<Persona> findAll();

    List<Persona> findByFirstName(String firstName);

    List<Person> findBySecondName(String secondName);
}
