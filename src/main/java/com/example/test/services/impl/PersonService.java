package com.example.test.services.impl;

import com.example.test.convert.PersonToPersona;
import com.example.test.dto.Persona;
import com.example.test.exceptions.PersonException;
import com.example.test.model.Person;
import com.example.test.repository.PersonRepository;
import com.example.test.services.PersonServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService implements PersonServiceI {

    private static final Logger log = LoggerFactory.getLogger(PersonService.class);

    private PersonRepository personRepository;

    private List<Person> lsPerson;

    private Person persons;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Persona findById(Long id) {

        var personToPersona = new PersonToPersona();
        try {
            persons = personRepository.findById(id).get();
        } catch (PersonException e) {
            log.warn("The DataBase can not process query  error 21 ", e.getMessage());
        }
        var p = personToPersona.transform(persons);
        return p;
    }

    @Override
    public List<Persona> findAll() {

        try {
            lsPerson = personRepository.findAll();
        } catch (PersonException e) {
            log.warn("The DataBase can not process query  error 20 ", e.getMessage());
        }
        return calculate(lsPerson);
    }

    @Override
    public List<Persona> findByFirstName(String firstName) {

        try {
            lsPerson = personRepository.findByFirstName(firstName);
        } catch (PersonException e) {
            log.warn("The DataBase can not process query  error 22 ", e.getMessage());
        }
        return calculate(lsPerson);
    }

    public List<Persona> calculate(List<Person> listPr) {

        var personToPersona = new PersonToPersona();
        List<Persona> listPerson = new ArrayList<>();
        for (Person p : listPr) {
            var persona = personToPersona.transform(p);
            listPerson.add(persona);
        }
        return listPerson;
    }

    @Override
    public List<Person> findBySecondName(String secondName) {
        return null;
    }
}
