package com.example.test.services.impl;

import com.example.test.convert.CarsToCar;
import com.example.test.dto.Car;
import com.example.test.exceptions.CarException;
import com.example.test.model.Cars;
import com.example.test.repository.CarRepository;
import com.example.test.services.CarServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarService implements CarServiceI {

    private static final Logger log = LoggerFactory.getLogger(CarService.class);

    private CarRepository carRepository;

    private Cars lCars;

    private List<Cars> listCars;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car findById(Long id) {

        var trs = new CarsToCar();
        try {
            Optional<Cars> lsCar = carRepository.findById(id);
            lCars = lsCar.get();
        } catch (CarException e) {
            log.warn("The DataBase can not process query  error 12 ", e.getMessage());
        }
        return trs.transform(lCars);
    }

    @Override
    public List<Car> findAll() {

        List<Car> listCar = new ArrayList<>();
        var carsToCar = new CarsToCar();
        try {
            listCars = carRepository.findAll();
        } catch (CarException e) {
            log.warn("The DataBase can not process query  error 10 ", e.getMessage());
        }
        for (Cars cars : listCars) {
            listCar.add(carsToCar.transform(cars));
        }
        return listCar;
    }

    @Override
    public List<Car> findByName(String name) {
        return null;
    }

    @Override
    public List<Car> findByCountry(String country) {
        return null;
    }

}
