package com.example.test.services;

import com.example.test.dto.Car;

import java.util.List;

public interface CarServiceI {

    Car findById(Long aLong);

    List<Car> findAll();

    List<Car> findByName(String name);

    List<Car> findByCountry(String country);
}
