package com.example.test.convert;

import com.example.test.dto.Car;
import com.example.test.model.Cars;

public class CarsToCar {

    public Car transform (Cars cars){
        var car = new Car();
        car.setCarName(cars.getName());
        car.setId(cars.getId());
        return car;
    }
}
