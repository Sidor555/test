package com.example.test.convert;

import com.example.test.dto.Persona;
import com.example.test.model.Person;

public class PersonaToPerson {

    public Person transform(Persona persona) {
        var person = new Person();
        person.setId(persona.getPersons_Id());
        person.setFirstName(persona.getPersonName());
        return person;
    }
}
