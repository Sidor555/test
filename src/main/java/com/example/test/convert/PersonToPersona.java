package com.example.test.convert;

import com.example.test.dto.Persona;
import com.example.test.model.Cars;
import com.example.test.model.Person;

import java.util.stream.Collectors;

public class PersonToPersona {

    public Persona transform(Person person) {
        var persona = new Persona();
        persona.setPersons_Id(person.getId());
        persona.setPersonName(person.getFirstName());
        persona.setCarNameList(person.getCarsList().stream().map(Cars::getName).collect(Collectors.toList()));
        return persona;
    }
}
