package com.example.test.convert;

import com.example.test.dto.Car;
import com.example.test.model.Cars;

public class CarToCars {

    public Cars transform(Car car) {
        var cars = new Cars();
        cars.setName(car.getCarName());
        return cars;
    }
}
