package com.example.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Car {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("cars")
    private String carName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", carName='" + carName + '\'' +
                '}';
    }
}



