package com.example.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Persona {

    @JsonProperty("id")
    private Long persons_Id;
    @JsonProperty("persona")
    private String personName;
    @JsonProperty("cars")
    private List<String> carNameList;

    public void setCarNameList(List<String> carNameList) {
        this.carNameList = carNameList;
    }

    public Long getPersons_Id() {
        return persons_Id;
    }

    public void setPersons_Id(Long persons_Id) {
        this.persons_Id = persons_Id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "persons_Id=" + persons_Id +
                ", personName='" + personName + '\'' +
                ", carNameList=" + carNameList +
                '}';
    }
}
