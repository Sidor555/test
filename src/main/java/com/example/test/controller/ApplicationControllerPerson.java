package com.example.test.controller;

import com.example.test.dto.Persona;
import com.example.test.services.impl.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8181")
@RestController
@RequestMapping("/api")
public class ApplicationControllerPerson {
    
    private PersonService personService;

    public ApplicationControllerPerson( PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = "/persons/{id}")
    public ResponseEntity<Persona> getById(@PathVariable(name = "id") Long id) {
        var persona = personService.findById(id);

        return persona != null
                ? new ResponseEntity<>(persona, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/persons")
    public ResponseEntity<List<Persona>> getAll() {
        List<Persona> persons = personService.findAll();

        return persons != null && !persons.isEmpty()
                ? new ResponseEntity<>(persons, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/persons/list/{name}",  method = RequestMethod.GET)
    public ResponseEntity<List<Persona>> getByName(@PathVariable(name = "name") String name) {
        var persona = personService.findByFirstName(name);

        return persona != null
                ? new ResponseEntity<>(persona, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
