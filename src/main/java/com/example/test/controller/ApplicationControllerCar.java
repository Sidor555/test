package com.example.test.controller;

import com.example.test.dto.Car;
import com.example.test.dto.Persona;
import com.example.test.services.impl.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "http://localhost:8181")
@RestController
@RequestMapping("/api")
public class ApplicationControllerCar {

    private CarService carService;

    public ApplicationControllerCar(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(value = "/cars/{id}")
    public ResponseEntity<Car> getById(@PathVariable(name = "id") Long id) {
        var persona = carService.findById(id);

        return persona != null
                ? new ResponseEntity<>(persona, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/cars")
    public ResponseEntity<List<Car>> getAll() {
        List<Car> persons = carService.findAll();

        return persons != null && !persons.isEmpty()
                ? new ResponseEntity<>(persons, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
