package com.example.test.exceptions;

public class PersonException extends RuntimeException {
    public PersonException(String errorMessage) {
        super(errorMessage);
    }
}
