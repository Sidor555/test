package com.example.test.exceptions;

public class CarException extends RuntimeException {
    public CarException(String errorMessage) {
        super(errorMessage);
    }
}
