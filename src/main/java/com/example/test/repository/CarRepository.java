package com.example.test.repository;


import com.example.test.model.Cars;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Cars, Long> {
    @Override
    Optional<Cars> findById(Long aLong);

    @Override
    List<Cars> findAll();

    List<Cars> findByName(String name);

    List<Cars> findByCountry(String country);
}
