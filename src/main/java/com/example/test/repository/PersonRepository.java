package com.example.test.repository;

import com.example.test.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {
    @Override
    Optional<Person> findById(Long id);

    @Override
    List<Person> findAll();

    List<Person> findByFirstName(String firstName);

    List<Person> findBySecondName(String secondName);
}
